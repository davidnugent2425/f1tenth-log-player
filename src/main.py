import pyglet
from f1tenth_player import F1TenthVideoPlayer
import os
import time

SRC_PATH = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(SRC_PATH, '..')
MAP_PATH = os.path.join(PROJECT_PATH, 'resources', 'maps', 'SILVERSTONE')

if __name__ == "__main__":
    sample_log = os.path.join(PROJECT_PATH, 'resources', 'examples', 'logs', 'FAIvsLK.jsonl')

    player = F1TenthVideoPlayer(sample_log, MAP_PATH, 1280, 720)
    pyglet.clock.schedule_interval(player.update, 0.005)
    time.sleep(5)
    pyglet.app.run()
